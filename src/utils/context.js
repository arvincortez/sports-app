import React, { useState, useContext, useEffect } from 'react'
import { useCallback } from 'react'

const url = 'https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t='
const AppContext = React.createContext()

const AppProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [searchTerm, setSearchTerm] = useState('LA');
  const [teams, setTeams] = useState([]);

  const fetchTeams = useCallback(async () => {
    setLoading(true);
    try {
      const response = await fetch(`${url}${searchTerm}`)
      const data = await response.json();
      const {teams} = data;
      if(teams){
        const newTeams = teams.map((item)=>{
          const {idTeam, strTeam, strTeamBadge, strSport, strLeague, strLeague2, strStadium, strDescriptionEN} = item;
          return{
            id:idTeam,
            name:strTeam, 
            img:strTeamBadge,
            sport: strSport,
            league1: strLeague,
            league2: strLeague2,
            stadium: strStadium,
            description: strDescriptionEN,
        }
        })
        setTeams(newTeams);
      }
      else{
        setTeams([]);
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  },[searchTerm])
  useEffect(()=>{
    fetchTeams();
  },[searchTerm, fetchTeams])
  return <AppContext.Provider 
    value={{
      loading,
      teams,
      setSearchTerm,
  }}>{children}</AppContext.Provider>
}
// make sure use
export const useGlobalContext = () => {
  return useContext(AppContext)
}

export { AppContext, AppProvider }
