import {SEARCH} from './actions'
// import fetchData from './useFetch'

const reducer = (state, action) => {
    if(action.type === SEARCH){
        return {...state, team:action.payload, loading:false}
    }
}