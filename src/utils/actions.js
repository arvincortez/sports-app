export const SEARCH = 'SEARCH';
export const LOADING = 'LOADING'


export const searchTeams = (searchTerm) => {
    return {type:SEARCH, payload:{searchTerm}}
}