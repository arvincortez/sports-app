import React from 'react'
import { Link } from 'react-router-dom'

const SingleTeam = ({img, name ,id , sport, league1, league2, stadium, description}) => {
  return (
    <article className="team">
      <div className='img-container'>
        <img src={img} alt={name} className="team-img"/>
      </div>
      <div className="team-footer">
        <h3>Team Name: {name}</h3>
        <h4>Sports: {sport}</h4>
        <p>First League: {league1}</p>
        <p>Second League {league2 = 'none'}</p>
        <p>Statium: {stadium}</p>
        <p>Description: {description}</p>
        <Link to={`/team/${id}`} className="btn btn-primary btn-details">
          Details
        </Link>
      </div>
    </article>
  )
}

export default SingleTeam