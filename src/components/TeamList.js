import React from 'react'
import SingleTeam from './SingleTeam'
import { useGlobalContext } from '../utils/context'
import '../style/team_players.css'

const TeamList = () => {
  const {loading, teams} = useGlobalContext();
  if(loading){
    return (
      <h4>Loading....</h4>
    )
  }
  if(teams.length < 1){
    return(
      <h2 className='section-title'>
        no team matched your search criteria
      </h2>
    )
  }
  return (
    <section className="section"> 
      <h2 className="section-title">
        {teams.length > 1?'Sports Teams' : 'Sports Team' }
      </h2>
      <div className="teams-center">
        {teams.map((team)=>{
          return <SingleTeam key={team.id} {...team}></SingleTeam>
        })}
      </div>
    </section>
  )
}

export default TeamList
