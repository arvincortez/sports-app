import React from 'react'
import { useGlobalContext } from '../utils/context'
import '../style/searchSelection.css'

const SearchForm = () => {
  const {setSearchTerm} = useGlobalContext();
  const searchValue = React.useRef('');
  React.useEffect(()=>{
    searchValue.current.focus();
  }, [])
  const searchTeam = () => {
    setSearchTerm(searchValue.current.value)
  }
  const handleSubmit = (e) => {
    e.preventDefault();
  }
  return (
    <section className="section section">
      <form className="search-form" onSubmit={handleSubmit}>
        <div className="form-control">
          <label htmlFor="name"> Search for a sports team: </label>
          <input type="text" id="name" ref={searchValue} onChange={searchTeam}/>
        </div>
      </form>
    </section >
  )
}

export default SearchForm
