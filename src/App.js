import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import logo from './logo.svg';
import NavBar from './components/NavBar'
import Home from './pages/Home'
import About from './pages/About'
import Error from './pages/Error'
import Player from './pages/Player'
import Team from './pages/Team'

function App() {
  return (
    <Router>
      <NavBar/>
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <Route path="/about">
           <About/>
        </Route>
        <Route path="/player/:{id}">
          <Player/>
        </Route>
        <Route path="/team/:{id}">
          <Team/>
        </Route>
        <Route path="*">
          <Error/>
         </Route>
       </Switch>
    </Router>
  );
}

export default App;
