import React from 'react'
import SearchSelection from '../components/SearchSelection'
import PlayerList from '../components/PlayerList'
import TeamList from '../components/TeamList'

const Home = () => {
  return (
    <main>
      <SearchSelection/>
      <TeamList />
    </main>
  )
}

export default Home