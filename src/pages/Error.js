import React from 'react'
import '../style/about.css'

const Error = () => {
  return (
    <section className="section about-section">
      <h1 className="section-title">Error!!</h1>
    </section>
  )
}

export default Error
