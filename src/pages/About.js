import React from 'react'
import '../style/about.css'

const About = () => {
  return (
    <section className="section about-section">
      <h1 className="section-title">About this App</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed aperiam consequuntur praesentium neque ducimus eaque aut odit explicabo magni in debitis deserunt voluptate laboriosam, iste nobis ea nisi, tempore sunt quidem laudantium culpa perferendis. Voluptatibus facere dolore laudantium rerum temporibus, ducimus rem? Dicta, est hic porro repudiandae nam voluptas earum perspiciatis culpa vero error consequuntur amet possimus beatae omnis facere accusantium placeat nihil unde.
        </p>
        <p>
        Pariatur nisi alias non animi. Magni hic dolor laudantium reiciendis, optio animi magnam. Commodi pariatur qui totam, iure ipsum corrupti, blanditiis veritatis temporibus, vitae iusto ipsa perferendis libero. Eos unde tempora repellendus optio totam ut inventore ullam, accusantium officia quas quibusdam quae quo corrupti delectus soluta explicabo est obcaecati sapiente quisquam illum! Fugit sequi perspiciatis quo rerum magni odit repellat minima soluta ducimus aperiam, cum nihil aliquid distinctio quibusdam ipsam voluptas, obcaecati delectus tenetur qui molestiae molestias. Dignissimos, dolorum eligendi.
        </p>
        <p>
        Iure repellat, vitae iusto nobis similique possimus maiores illo, error excepturi unde deleniti aspernatur odit asperiores molestias reprehenderit distinctio qui quod tenetur aliquid nihil eveniet autem. Vitae ducimus voluptates voluptatum provident obcaecati, iusto dolorem velit accusamus repellat aspernatur incidunt voluptatem ullam sapiente sequi officiis iure ipsam tempora minima distinctio. Sequi, excepturi? Recusandae hic in molestiae ea?
      </p>
    </section>
  )
}

export default About
